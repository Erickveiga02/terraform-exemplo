# IP do vcenter
variable "vsphere_server" {
    description = "Configurações do servidor vcenter"
    default = "ip"
  
}
# usuário
variable "vsphere_user" {
    description = " usuário para acesar o servidor vcenter"
    default = "user@domain"
  
}
#Senha
variable "vsphere_password" {
    description = "senha para acesso"
    default = "fanu3057#!"
  
}
# servidores dns
variable "virtual_machine_dns_servers" {
    type = "list"
    description = " lista de endereços dns para fixar no servidor"
    default = ["ip_dns"]
  
}
# hostname
variable "vsphere_hostname" {
    description = "nome do servidor"
    default = "linux_topzera"
  
}
# nome da minha maquina linux
variable "vsphere_name" {
    description = " nome para o servidor "
    default = "de manhã só tem fdp"
  
}
# vcpu
variable "vsphere_vcpu" {
    description = " quantidade de cpus do servidor"
    default = "3"
  
}
# memoria
variable "vsphere_memory" {
    description = " quantidade de memoria para maquina"
    default = "4096"
  
}
# dominio
variable "vsphere_domain" {
    description = "dominio local"
    default = "seu.domain.org"
  
}
# nome do template já criado
variable "vsphere_virtual_machine.template.id" {
    description = " nome do template para subir a máquina"
    default = "linux"
  
}


