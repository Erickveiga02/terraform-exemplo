provider "vsphere" {
    user = "${var.vsphere_user}"
    password = "${var.vsphere_password}"
    vsphere_vcenter = "${var.vsphere_server}"
     allow_unvrified_ssl = true
  
}